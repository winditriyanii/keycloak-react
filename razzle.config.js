'use strict';

/* eslint-disable prefer-object-spread */
const path = require('path');
const glob = require('glob');
const LoadableWebpackPlugin = require('@loadable/webpack-plugin');
const LoadableBabelPlugin = require('@loadable/babel-plugin');
const babelPresetRazzle = require('razzle/babel');
const CompressionPlugin = require('compression-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const JavaScriptObfuscator = require('webpack-obfuscator');
const ImageminPlugin = require('imagemin-webpack');
const PurgecssPlugin = require('purgecss-webpack-plugin');

module.exports = {
  plugins: ['scss'],
  modify: (config, { dev, target }) => {
    const appConfig = Object.assign({}, config);
    const isDev = dev === true;
    const isProd = dev === false;
    const isWeb = target === 'web';
    const plugins = [];

    // Disable sourceMap on production
    appConfig.devtool = isDev ? 'cheap-module-eval-source-map' : false;

    // Change the name of the server output in production.
    if (isWeb) {
      const filename = path.resolve(__dirname, 'build');

      plugins.push(
        new LoadableWebpackPlugin({
          outputAsset: false,
          writeToDisk: { filename },
        }),
      );

      appConfig.output.filename = isDev
        ? 'static/js/[name].js'
        : 'static/js/[name].[chunkhash:8].js';

      appConfig.optimization = Object.assign({}, appConfig.optimization, {
        runtimeChunk: true,
        splitChunks: {
          chunks: 'all',
          name: isDev,
          cacheGroups: {
            commons: {
              test: /[\\/]node_modules[\\/]/,
              name: 'vendors',
            },
          },
        },
      });
    }

    // Start gzip/brotli compression.
    const options = {
      brotli: true,
      gzip: true,
      compressionPlugin: {
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.(js|css|html|svg)$/,
        compressionOptions: { level: 9 },
        minRatio: 0.8,
      },
      brotliPlugin: {
        asset: '[path].br[query]',
        test: /\.(js|css|html|svg)$/,
        threshold: 10240,
        minRatio: 0.7,
      },
    };

    if (isWeb && isProd) {
      if (options.gzip) {
        plugins.push(new CompressionPlugin(options.compressionPlugin));
      }

      if (options.brotli) {
        plugins.push(new BrotliPlugin(options.brotliPlugin));
      }
    }

    // On production...
    if (isProd) {
      // start image compression on prod.
      plugins.push(
        new ImageminPlugin({
          bail: false, // Ignore errors on corrupted images
          cache: true,
          imageminOptions: {
            plugins: [
              ['gifsicle', { interlaced: true }],
              ['mozjpeg', { quality: 75, progressive: true }],
              ['pngquant', { quality: [0.75, 0.75] }],
              [
                'svgo',
                {
                  plugins: [
                    {
                      cleanupAttrs: true,
                      inlineStyles: true,
                      removeDoctype: true,
                      removeXMLProcInst: true,
                      removeComments: true,
                      removeMetadata: true,
                      removeTitle: true,
                      removeDesc: true,
                      removeUselessDefs: true,
                      removeXMLNS: false,
                      removeEditorsNSData: true,
                      removeEmptyAttrs: true,
                      removeHiddenElems: true,
                      removeEmptyText: true,
                      removeEmptyContainers: true,
                      removeViewBox: false,
                      cleanupEnableBackground: true,
                      minifyStyles: true,
                      convertStyleToAttrs: true,
                      removeUnknownsAndDefaults: true,
                      removeUselessStrokeAndFill: true,
                      removeUnusedNS: true,
                      cleanupIDs: true,
                      moveElemsAttrsToGroup: true,
                      moveGroupAttrsToElems: true,
                      collapseGroups: true,
                      removeRasterImages: true,
                      mergePaths: true,
                      sortAttrs: true,
                      removeScriptElement: true,
                    },
                  ],
                },
              ],
            ],
          },
        }),
      );

      // remove unused CSS using purgecss
      plugins.push(
        new PurgecssPlugin(
          {
            paths: glob.sync(path.resolve(__dirname, 'src/**/*'), {
              nodir: true,
            }),
          },
          {
            nodir: true,
          },
        ),
      );
    }

    appConfig.plugins.push(...plugins);

    return appConfig;
  },

  modifyBabelOptions: () => ({
    babelrc: false,
    presets: [babelPresetRazzle],
    plugins: [LoadableBabelPlugin],
  }),
};
