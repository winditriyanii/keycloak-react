import React from 'react';
import loadable from '@loadable/component';
import { RouteList } from 'modules/components/routes';

const routes = [
  {
    path: '/',
    exact: true,
    component: loadable(() => import('./home')),
  },
];

export default () => <RouteList routes={routes} />;
