import App from './app';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import React from 'react';
import { hydrate } from 'react-dom';

// We automatically serve in server so we don't need to have multiple conditions to serve
// using either render() or hydrate() on dev/prod env. Hydrate 💦 all the way!
hydrate(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root'),
);

if (module.hot) {
  module.hot.accept();
}
