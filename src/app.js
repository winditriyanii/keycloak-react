import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import Home from './pages/home';

class App extends Component {
  render() {
    return <Home />;
  }
}
export default App;
