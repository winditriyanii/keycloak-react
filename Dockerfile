FROM node:10.16.3-alpine as build
MAINTAINER SRE <sre@amarbank.co.id>

ARG APP_ENV

RUN mkdir -p /app
ADD . /app

WORKDIR /app

RUN apk update && \
    apk add --no-cache alpine-sdk automake make gcc g++ autoconf nasm zlib-dev nodejs-npm git python libpng libpng-dev libjpeg libjpeg-turbo-dev libc6-compat&& \
    yarn add react-helmet && \
    yarn install && \
    yarn build:prod && \
    yarn cache clean 

RUN rm -rf /tmp/* && \
    rm -rf /var/cache/apk/*

FROM node:10.16.3-alpine

RUN apk add --update tzdata && \
    apk add ca-certificates && \
    ls /usr/share/zoneinfo && \
    cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && \
    echo "Asia/Jakarta" >  /etc/timezone && \
    apk del tzdata && \
    rm -rf /tmp/* && \
    rm -rf /var/cache/apk/* 

COPY --from=build /app /app

#RUN ln -s /usr/local/lib/node_modules/ts-node/dist/bin.js /usr/local/bin/ts-node

WORKDIR /app
EXPOSE 3000

CMD ["yarn", "start:prod"]