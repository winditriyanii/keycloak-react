# keycloak-react

### Getting Started

- Clone repo from this URI
- `npm` install or `yarn`

---

### Installation

- npm `yarn` or `npm`

### Development (Clien-side rendering)

- run `yarn` start or `npm` start.
- Then open http://localhost:3000/ to see your app.

### Production (also for testing SSR/Pre-rendering locally)

- run `yarn` **build:prod** or `npm` **build:prod** to production enviroment
- Then run `yarn` **start:prod** or `npm` **build:prod** to production enviroment

---
